import React from 'react';
import classes from './CityForm.css';
import FaSearch from 'react-icons/lib/fa/search';
import TiTimes from 'react-icons/lib/ti/times';



class CityForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            city: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            city: event.target.value
        });
    }

    handleSubmit = async (event) =>  {
        try {
            event.preventDefault();
            if (this.state.city.trim() === ''){
                return
            } else {
                let weather = await this.props.search(this.state.city.trim());
                if (!weather) {
                    this.setState({city: 'Unable to get weather'});
                    setTimeout(()=> {
                        this.setState({city: ''});
                        this.handleReset();
                    },2000);
                }
                // console.log('weather',weather);
            }
        } catch (e) {
            this.setState({city: 'Unable to get weather'});
            setTimeout(()=> {
                this.setState({city: ''});
                this.handleReset();
            },2000);
            console.log(e)
        }
    }

    handleReset = () => {
        this.props.reset();
        this.setState({
            city: '',
        })
    }

    render() {
        return (
            <form className={classes.form} onSubmit={this.handleSubmit}>
                <div className={classes.container}>
                    <span className={classes.searchIcon}><FaSearch /></span>
                    <input type="input" className={classes.input} name="city" autoComplete="off" value={this.state.city} placeholder="Enter A City" onChange={this.handleChange}/>
                    <span className={classes.exitIcon} onClick={this.handleReset}><TiTimes /></span>
                </div>                
            </form>
        )
    }
    
}

export default CityForm;