import React from 'react';
import classes from './CurrentWeather.css';

const currentWeather = (props) => {
    const capitalizeFirstLetters = (str) => {
        return str.toLowerCase().replace(/^\w|\s\w/g, (letter) => {
            return letter.toUpperCase();
        });
      };

      let upper =capitalizeFirstLetters(props.description)

      
    
    return (
        <div className={classes.current}>
            <div className={classes.degrees}>
                <h1 className={classes.temp}>{Math.round(props.temp)}</h1>
                <p className={classes.degSymbol}>&#176;</p>
            </div>
            <div className={classes.description}>
                <p className={classes.f}>F</p>
                <p className={classes.p}>{upper}</p>
                <p className={classes.p}>{props.humidity}% Humidity</p>
            </div>
        </div>
    )
}

export default currentWeather;