import React from 'react';
import Day from './Day/Day';

const weather = (props) => props.keys.map((date, index) => {
    if (index !== 5) {
        return  <Day key={index}
            date={date}
            high={props.highs[date]}
            low={props.lows[date]}
            icon={props.icon[date]}
        />
    }
    
 })

export default weather;