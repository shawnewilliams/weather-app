import React from 'react';
import classes from './Day.css';

const day = (props) => {
    return (
    <div className={classes.day}>
       <p className={classes.date}>{props.date}</p>
       <img className={classes.icon} src={`https://openweathermap.org/img/w/${props.icon}.png`} alt="Weather Icon"/>
       <p className={classes.high}>{Math.round(props.high)}</p>
       <p className={classes.low}>{Math.round(props.low)}</p>
    </div>
    )
}

export default day;