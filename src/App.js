import React, { Component } from 'react';
import CityForm from './CityForm/CityForm';
import axios from 'axios';
import cloud1 from './images/cloud1.png';
import cloud2 from './images/cloud2.png';
import Weather from './Weather/Weather';
import CurrentWeather from './CurrentWeather/CurrentWeather';
import classes from './App.css';
import Draggable from 'react-draggable-component';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weather: {}
    };
  }

  getforecast = (data) => {
    let forecast = data.list.map((list) => {
      let date = new Date(list.dt * 1000).toString().split(' ');
      return {
        tempMax: list.main.temp_max,
        tempMin: list.main.temp_min,
        icon: list.weather[0].icon,
        humidity: list.main.humidity,
        dt: list.dt,
        day: date[1] + ' ' + date[2],
        date: date.join(' '),
        time: date[4],
        description: list.weather[0].description
      }
    });
    return forecast;
  }

  getLows = (data) => {
    let day = {}
    for (let item of data) {
      if(!day[item.day]) {
        day[item.day] = item.tempMin;
      } else if(day[item.day] > item.tempMin) {
        day[item.day] = item.tempMin
      }
    }
    return day;
  }

  getHighs = (data) => {
    let day = {}
    for (let item of data) {
      if(!day[item.day]) {
        day[item.day] = item.tempMax;
      } else if(day[item.day] < item.tempMax) {
        day[item.day] = item.tempMax
      }
    }
    return day;
  }

  getIcon = (data) => {
    let day = {}
    for (let item of data) {
      
      if(!day[item.day]) {
        day[item.day] = [item.icon];
      } else {
        day[item.day].push(item.icon)
      }
    }
    let icon = {}
    console.log("icons",day)
    for(let item in day) {
      let index = Math.floor(day[item].length / 2)
      if(day[item].length < 8) {
        icon[item] = day[item][0];
      } else {
        icon[item] = day[item][index];
      }
      
    }
    console.log("icon", icon)
    return icon;
  }

  getCurrent = (data) => {
    let current = 
       {
        temp: data.main.temp,
        tempMax: data.main.temp_max,
        tempMin: data.main.temp_min,
        icon: data.weather[0].icon,
        humidity: data.main.humidity,
        dt: data.dt,
        description: data.weather[0].description
      }
    return current;
  }

  searchHandler = async (city) => {
    try {
      let forecastData = await axios(`https://api.openweathermap.org/data/2.5/forecast?q=${city}&units=imperial&APPID=b9b7d4cf5daab39b458d23e8b192182e`);
      let currentData = await axios(`https://api.openweathermap.org/data/2.5/weather?q=${city}&units=imperial&APPID=b9b7d4cf5daab39b458d23e8b192182e`);
      
      console.log('ForecastData', forecastData);
      console.log('currentData', currentData);
      
      let forecast =  this.getforecast(forecastData.data);
      let current = this.getCurrent(currentData.data);
      let lows = this.getLows(forecast);
      let highs = this.getHighs(forecast); 
      let icon = this.getIcon(forecast);
      let weather = {
        forecast,
        current,
        lows,
        highs,
        icon
      };
      this.setState({ weather });
      console.log('combined weather',weather)
      return weather;
    } catch (e){
      return null;
    } 
  }

  resetStateHandler = () => {
    this.setState({weather: {}})
  }

  render() {

    let displayWeather = null;
    let displayTemp = null;
    if (this.state.weather.highs) {
      let keys = Object.keys(this.state.weather.highs)
      displayWeather = (
      <Weather 
        keys={keys} 
        icon={this.state.weather.icon}
        highs={this.state.weather.highs}
        lows={this.state.weather.lows} />
      )

      displayTemp = (
        <CurrentWeather 
          temp={this.state.weather.current.temp}
          humidity={this.state.weather.current.humidity}
          description={this.state.weather.current.description}/>
      )
    }

    return (
      <div>
        <Draggable className={classes.draggable}>
        <div className={classes.App}>
            <CityForm search={this.searchHandler} reset={this.resetStateHandler}/>
            {displayTemp}
            <div className={classes.weather}>
              {displayWeather}
            </div>
        </div>
        </Draggable>
        <img className={classes.cloud1} src={cloud1} alt="Cloud 1"/>
        <img className={classes.cloud2} src={cloud2} alt="Cloud 1"/>
      </div>
    );
  }
}

export default App;
